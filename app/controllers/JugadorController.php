<?php
namespace App\Controllers;

use \App\Models\Jugador;
use \App\Models\Puesto;
require_once '../app/models/Jugador.php';
require_once '../app/models/Puesto.php';

class JugadorController
{

    function __construct(){}

    public function index()
    {
        $pagesize = 4;
        $jugadores = Jugador::paginate($pagesize);
        $rowCount = Jugador::rowCount();

        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        require "../app/views/jugador/index.php";
    }

    public function create()
    {
        $puestos = Puesto::all();
        require '../app/views/jugador/create.php';
    }

    public function store()
    {
        $jugador = new Jugador();
        $jugador->nombre = $_REQUEST['nombre'];
        $jugador->nacimiento = $_REQUEST['nacimiento'];
        $jugador->id_puesto = $_REQUEST['puesto'];
        /*echo "<pre>";
        var_dump($jugador);
        exit();*/
        $jugador->insert();
        header('Location:/jugador');
    }

    public function titulares()
    {
        require '../app/views/jugador/titulares.php';
    }

    public function titular($arguments)
    {
        $flag = false;
        $id = (int) $arguments[0];
        $jugador = Jugador::find($id);
        if(isset($_SESSION["titulares"])){
            foreach ($_SESSION["titulares"] as $key => $persona) {
                if ($persona->id == $id) {
                    $flag=true;
                }
            }
            if(!$flag)
            {
                $_SESSION["titulares"][] = $jugador;
            }
        }else {
            $_SESSION["titulares"][] = $jugador;
        }

        /*unset($_SESSION["titulares"]);
        echo "<pre>";
        var_dump($_SESSION["titulares"]);
        exit();*/

        header('Location:/jugador/titulares');
    }

    public function quitar($arguments)
    {
        $id = (int) $arguments[0];
        foreach ($_SESSION["titulares"] as $key => $persona) {
            if ($persona->id == $id) {
                unset($_SESSION["titulares"][$key]);
            }
        }
        header('Location:/jugador/titulares');
    }

}
