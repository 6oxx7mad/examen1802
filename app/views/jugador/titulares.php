<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Titulares</h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Nacimiento</th>
          <th>Puesto</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($_SESSION["titulares"] as $jugador): ?>
          <tr>
          <td><?php echo $jugador->id ?></td>
          <td><?php echo $jugador->nombre ?></td>
          <td><?php echo date("d/m/Y", strtotime($jugador->nacimiento)) ?></td>
          <td><?php echo $jugador->id_puesto ?></td>
          <td>
            <a class="btn btn-primary" href="/jugador/quitar/<?php echo $jugador->id ?>">QUITAR</a>
          </td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    <hr>
    </div>

  </main>
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
