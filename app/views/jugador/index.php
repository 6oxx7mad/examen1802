<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Jugadores</h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Nacimiento</th>
          <th>Puesto</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($jugadores as $jugador): ?>
          <tr>
          <td><?php echo $jugador->id ?></td>
          <td><?php echo $jugador->nombre ?></td>
          <td><?php echo date("d/m/Y", strtotime($jugador->nacimiento)) ?></td>
          <td><?php echo $jugador->puesto->nombre ?></td>
          <td>
            <a class="btn btn-primary" href="/jugador/titular/<?php echo $jugador->id ?>">TITULAR</a>
          </td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    <?php for ($i=1; $i <= $pages; $i++) { ?>
      <?php if ($i != $page): ?>
      <a href="/jugador?page=<?php echo $i ?>" class="btn">
        <?php echo $i ?>
      </a>
      <?php else: ?>
        <span class="btn">
        <?php echo $i ?>
        </span>
      <?php endif ?>
    <?php } ?>
    <hr>

    <a href="/jugador/create">Nuevo</a>
    </div>

  </main>
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
