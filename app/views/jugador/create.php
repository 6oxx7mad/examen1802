<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Alta de jugador</h1>

      <form method="post" action="/jugador/store">

        <div class="form-group">
          <label>Nombre</label>
          <input type="text" name="nombre" class="form-control">
        </div>
        <div class="form-group">
          <label>Nacimiento</label>
          <input type="text" name="nacimiento" class="form-control">
        </div>
        <div class="form-group">
            <label>Puesto</label>
            <select name="puesto">
                <?php foreach($puestos as $puesto): ?>
                    <option name="<?php echo $puesto->id; ?>" value="<?php echo $puesto->id; ?>">
                        <?php echo $puesto->nombre; ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Enviar</button>
      </form>
    </div>
  </main>
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
